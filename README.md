## Download data test

These data are to test the workflow [snakemake_rapidrun_swarm](https://gitlab.mbb.univ-montp2.fr/edna/snakemake_rapidrun_swarm)

```
wget -c https://gitlab.mbb.univ-montp2.fr/edna/test_metabarcoding_data/-/raw/master/test_rapidrun_data.tar.gz -O - | tar -xz -C .
```