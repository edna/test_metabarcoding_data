import argparse
import random
from Bio.Seq import Seq




parser = argparse.ArgumentParser(description='VCFsynonymous - detect synonymous genetic variants in VCF')
parser.add_argument("-5","--primer5", type=str, help='sequence of primer 5 prime')
parser.add_argument("-3","--primer3", type=str, help='sequence of primer 3 prime')
parser.add_argument("-t","--tag", type=str, help='sequence of tag')
parser.add_argument("-b","--barcode",type=str, help='sequence of barcode')
parser.add_argument("-n", "--number_sequence", type=int, help='number of sequences to generate')
parser.add_argument("-i", "--id_flowcell", type=int, help='Id of the flowcell for header')
parser.add_argument("-r", "--read_pair", type=int, help='Pair of the read is reverse (2) or forward (1)')


args = parser.parse_args()


primer5= args.primer5
primer3 = args.primer3
tag = args.tag
barcode = args.barcode
n_seq = args.number_sequence
id_flowcell = args.id_flowcell
pair = args.read_pair

seq = Seq(primer5)
primer5_rc = str(seq.reverse_complement())
seq = Seq(primer3)
primer3_rc = str(seq.reverse_complement())
seq = Seq(tag)
tag_rc = str(seq.reverse_complement())
seq = Seq(barcode)
barcode_rc = str(seq.reverse_complement())




if pair == 2:
    seq_r2 = tag + primer3 + barcode_rc + primer5_rc + tag_rc
    seq_r2 = seq_r2.replace('M', random.choice('AC')).replace('R', random.choice('AG')).replace('W', random.choice('AT')).replace('S', random.choice('CG')).replace('Y', random.choice('CT')).replace('K',random.choice('GT')).replace('V', random.choice('ACG')).replace('H', random.choice('ACT')).replace('D', random.choice('AGT')).replace('B', random.choice('CGT')).replace('N', random.choice('AGTC'))
    if len(seq_r2) < 125:
        seq_r2 +="A"*(125-len(seq_r2))
    else:
        seq_r2=str(seq_r2[0:125]) 
    for i in range(1,(n_seq+1)):
        header = "@MISEQ:1:flowcell"+ str(id_flowcell)+":1"+":1:"+str(i)+" 2:N:0:CGATGT"
        print(header)
        print(seq_r2)
        print("+")
        print("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB")
else: 
    seq_r1 = tag + primer5 + barcode + primer3_rc + tag_rc
    seq_r1 = seq_r1.replace('M', random.choice('AC')).replace('R', random.choice('AG')).replace('W', random.choice('AT')).replace('S', random.choice('CG')).replace('Y', random.choice('CT')).replace('K',random.choice('GT')).replace('V', random.choice('ACG')).replace('H', random.choice('ACT')).replace('D', random.choice('AGT')).replace('B', random.choice('CGT')).replace('N', random.choice('AGTC'))
    if len(seq_r1) < 125:
        seq_r1 +="A"*(125-len(seq_r1))
    else:
        seq_r1=str(seq_r1[0:125])
    for i in range(1,(n_seq+1)):
        header = "@MISEQ:1:flowcell"+ str(id_flowcell)+":1"+":1:"+str(i)+" 1:N:0:CGATGT"
        print(header)
        print(seq_r1)
        print("+")
        print("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB")



