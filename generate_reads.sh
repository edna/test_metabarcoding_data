rm resources/test/test_rapidrun_data/ngs/runs/*.fastq.gz
## chond blue shark run1 flowcell1 plaque1 | 12 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CTCAAAATAATTTACCCTTTTTCATAAATACATTTCTCCAACAA -n 12 -i 1 -r 1 > resources/test/test_rapidrun_data/ngs/runs/run1_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CTCAAAATAATTTACCCTTTTTCATAAATACATTTCTCCAACAA -n 12 -i 1 -r 2 > resources/test/test_rapidrun_data/ngs/runs/run1_R2.fastq.gz
## chond blacktip shark run1 flowcell2 plaque2 | 12 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCTC -3 CATGTTACGACTTGCCTCCTC -t GGAATGAG -b CTCAAAAACAACCTATCCTTTTTCATAAACACATCTCTTTAATAA -n 12 -i 2 -r 1 >> resources/test/test_rapidrun_data/ngs/runs/run1_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCTC -3 CATGTTACGACTTGCCTCCTC -t GGAATGAG -b CTCAAAAACAACCTATCCTTTTTCATAAACACATCTCTTTAATAA -n 12 -i 2 -r 2 >> resources/test/test_rapidrun_data/ngs/runs/run1_R2.fastq.gz
## chond blue shark run2 flowcell1 plaque1 | 12 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CTCAAAATAATTTACCCTTTTTCATAAATACATTTCTCCAACAA -n 12 -i 1 -r 1 > resources/test/test_rapidrun_data/ngs/runs/run2_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CTCAAAATAATTTACCCTTTTTCATAAATACATTTCTCCAACAA -n 12 -i 1 -r 2 > resources/test/test_rapidrun_data/ngs/runs/run2_R2.fastq.gz
## teleo european bass run2 flowcell2 plaque2 | 15 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t GGAATGAG -b CCCCAAGTCTACCAATACCGTATAACTAATACGCTAAAACTTGCAAAGGGGAGATAAGTC -n 15 -i 2 -r 1 >> resources/test/test_rapidrun_data/ngs/runs/run2_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t GGAATGAG -b CCCCAAGTCTACCAATACCGTATAACTAATACGCTAAAACTTGCAAAGGGGAGATAAGTC -n 15 -i 2 -r 2 >> resources/test/test_rapidrun_data/ngs/runs/run2_R2.fastq.gz


## teleo european bass run5 flowcell1 plaque1 | 11 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CCCCAAGTCTACCAATACCGTATAACTAATACGCTAAAACTTGCAAAGGGGAGATAAGTC -n 11 -i 1 -r 1 > resources/test/test_rapidrun_data/ngs/runs/run5_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t AACAAGCC -b CCCCAAGTCTACCAATACCGTATAACTAATACGCTAAAACTTGCAAAGGGGAGATAAGTC -n 11 -i 1 -r 2 > resources/test/test_rapidrun_data/ngs/runs/run5_R2.fastq.gz
## teleo electro eel run5 flowcell2 plaque2 | 12 sequences
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t GGAATGAG -b CCCCTAATTTTCTAATATTTGTAACTAACCAACTAATCTACCCTCGGGGAGGCAAGTCGTAA -n 12 -i 2 -r 1 >> resources/test/test_rapidrun_data/ngs/runs/run5_R1.fastq.gz
python3 generate_read.py -5 ACACCGCCCGTCACTCT -3 CTTCCGGTACACTTACCATG -t GGAATGAG -b CCCCTAATTTTCTAATATTTGTAACTAACCAACTAATCTACCCTCGGGGAGGCAAGTCGTAA -n 12 -i 2 -r 2 >> resources/test/test_rapidrun_data/ngs/runs/run5_R2.fastq.gz

## mammals sea otters run4 flowcell1 plaque1 | 15 sequences
python3 generate_read.py -5 CCGCCCGTCACYCTCCT -3 GTAYRCTTACCWTGTTACGAC -t AACAAGCC -b TGAGCAATATACCCAAATACTACATAATTTATAAACTGAACTAAAGCAAGAGGAGACAA -n 15 -i 1 -r 1 > resources/test/test_rapidrun_data/ngs/runs/run4_R1.fastq.gz
python3 generate_read.py -5 CCGCCCGTCACYCTCCT -3 GTAYRCTTACCWTGTTACGAC -t AACAAGCC -b TGAGCAATATACCCAAATACTACATAATTTATAAACTGAACTAAAGCAAGAGGAGACAA -n 15 -i 1 -r 2 > resources/test/test_rapidrun_data/ngs/runs/run4_R2.fastq.gz

## mammals Harbor porpoises run4 flowcell2 plaque2 | 14 sequences
python3 generate_read.py -5 CCGCCCGTCACYCTCCT -3 GTAYRCTTACCWTGTTACGAC -t GGAATGAG -b CAAGTATCACAGCAGAGCCTTAGATTACTAATCCCTGCTAAGCAAGCATACAAGAGGAGACAA -n 14 -i 2 -r 1 >> resources/test/test_rapidrun_data/ngs/runs/run4_R1.fastq.gz
python3 generate_read.py -5 CCGCCCGTCACYCTCCT -3 GTAYRCTTACCWTGTTACGAC -t GGAATGAG -b CAAGTATCACAGCAGAGCCTTAGATTACTAATCCCTGCTAAGCAAGCATACAAGAGGAGACAA -n 14 -i 2 -r 2 >> resources/test/test_rapidrun_data/ngs/runs/run4_R2.fastq.gz

## vert pandas  run3 flowcell1 plaque 1 | 18 sequences
python3 generate_read.py -5 TAGAACAGGCTCCTCTAG -3 TTAGATACCCCACTATGC -t AACAAGCC -b GTGGGTTTAGGCACCGCCAAGTCCTTAGAGTTTTAAGCGTTTGTGCTCGTAGTTCTCGGGCGGATGCTTCAGTACGAGATAGCATCAAGATTTAGGGCTAA -n 18 -i 1 -r 1 > resources/test/test_rapidrun_data/ngs/runs/run3_R1.fastq.gz
python3 generate_read.py -5 TAGAACAGGCTCCTCTAG -3 TTAGATACCCCACTATGC -t AACAAGCC -b GTGGGTTTAGGCACCGCCAAGTCCTTAGAGTTTTAAGCGTTTGTGCTCGTAGTTCTCGGGCGGATGCTTCAGTACGAGATAGCATCAAGATTTAGGGCTAA -n 18 -i 1 -r 2 > resources/test/test_rapidrun_data/ngs/runs/run3_R2.fastq.gz
## vert wolf run3 flowcell2 plaque 2 | 17 sequences
python3 generate_read.py -5 TAGAACAGGCTCCTCTAG -3 TTAGATACCCCACTATGC -t GGAATGAG -b AGGGATATAAAGCACAGCCACGTCCTTTGAGATTTAAGCTGTTGCTAGTAGTTCTCTGGTGAATTATTTTGTTATAGACTGATTTATGTTTAGGGCTAA -n 17 -i 2 -r 1 >> resources/test/test_rapidrun_data/ngs/runs/run3_R1.fastq.gz
python3 generate_read.py -5 TAGAACAGGCTCCTCTAG -3 TTAGATACCCCACTATGC -t GGAATGAG -b AGGGATATAAAGCACAGCCACGTCCTTTGAGATTTAAGCTGTTGCTAGTAGTTCTCTGGTGAATTATTTTGTTATAGACTGATTTATGTTTAGGGCTAA -n 17 -i 2 -r 2 >> resources/test/test_rapidrun_data/ngs/runs/run3_R2.fastq.gz
